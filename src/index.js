import logo, { ReactComponent } from './logo.svg';
import './App.css';
import React from 'react';
import reactDom from 'react-dom';

//declaration des jsx
const mois = ['JANVIER','FEVRIER','MARS','AVRIL','MAI','JUIN','JUILLET','AOUT','SEPTEMBRE','OCTOBRE','NOVEMBRE','DECEMBRE'];
const jour = ['DIMANCHE','LUNDI','MARDI','MERCREDI','JEUDI','VENDREDI','SAMEDI'];
const heures = Array(24).fill(null).map((value, index) => { return (<option key={index} value={index}>{index}</option>)});
const minutes = Array(60).fill(null).map((value, index) => { return (<option key={index} value={index}>{index}</option>)});
//fonction de la periode
function Periode(props) {
  if(props.date.getHours() >= 0 && props.date.getHours() <= 12){
   return ("MATIN");
    }
    if(props.date.getHours() > 12 && props.date.getHours() < 15){
   return ("APRES-MIDI");
    }
    if(props.date.getHours() >= 15){
    return ("SOIR");
    }
    return null;
  }

  //foncion de modification
  class Modification extends React.Component { 
    constructor(props) {
      super(props);
      this.state = {date:new Date()}
    };
  handleminute = e => {
    const minute = e.target.value;
    const date = this.state.date;
    this.state.date.setMinutes(minute);
    this.state.date.setSeconds(0);
    this.state.date({ date: date});
  }


  handleheure = e => {
  const heure = e.target.value;
  const date = this.state.date;
  this.state.date.setHours(heure);
  this.state.date.setMinutes(0);
  this.state.date({ date: date });
  }
  

  handleDate = e => {
    const datte = e.target.value;
    const date = this.state.date;
    date.setDate(datte);
    this.setState({ date: date});
  }
  }
  

// principal composant
class Clock extends React.Component {
  constructor(props) {
  super(props);
  this.state = {date: new Date()}
}
tick = () => {
  const seconde = this.state.date.getSeconds();
  const date = this.state.date;
  date.setSeconds(seconde + 1);
  this.setState({ date: date});
}

componentDidMount() {
  this.timerID = setInterval(
    () => this.tick(), 1000
  );
}
componentWillUnmount() {
  clearInterval(this.timerID);
}


handleminute = e => {
  const minute = e.target.value;
  const date = this.state.date;
  date.setMinutes(minute);
  date.setSeconds(0);
  this.setState({ date: date});
}


handleheure = e => {
const heure = e.target.value;
const date = this.state.date;
date.setHours(heure);
date.setMinutes(0);

this.setState({ date: date });
}


handleDate = e => {
  let datte =e.target.value;
  datte= new Date(datte)
  const date = this.state.date;
  date.setDate(datte.getUTCDate());
  date.setMonth(datte.getMonth())
  date.setFullYear(datte.getFullYear())
  this.setState({ date: date});
}

render(){
  return (
<p>

  <label>Modifier L'Heure:</label>
  <select name="heure" onChange={(e) => this.handleheure(e)} >
   {heures}
  </select><br/><br/>


  <label>Modifier la Minute:</label>
  <select name="minute" onChange={(e) => this.handleminute(e)} >
   {minutes}
   </select><br/><br/>


    <div>
      <h1> {jour[this.state.date.getDay()]} <br/>
      <Periode date={this.state.date}/>
      </h1>
      <h1>{this.state.date.getHours()}:{this.state.date.getMinutes()} <br/> 
      {this.state.date.getUTCDate()} {mois[this.state.date.getUTCMonth()]} {this.state.date.getUTCFullYear()}
      </h1>
    </div> <br/><br/>
    Modifier la Date:
   <input type="Date" onChange={(e) => this.handleDate(e)}/>

  </p>
  );
  }
}
reactDom.render(
  <Clock />,
  document.getElementById('root')
);

